create table cliente (
	codigo int,
	nome varchar(60),
	primary key(codigo)
)

create table produto (
	codigo int,
	nome varchar(60),
	primary key(codigo)
)

create table compra (
	numero int,
	data date,
	descricao varchar(60),
	codclie int,
	primary key(numero),
	foreign key (codclie) references pessoa(codigo)
)

create table itemCompra (
	coditem int,
	numcompra int,
	primary key(coditem,numcompra),
	foreign key (coditem) references produto(codigo),
	foreign key (numcompra) references compra(numero)
)
