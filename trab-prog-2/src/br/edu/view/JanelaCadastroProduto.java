package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.repository.BancoDados;

public class JanelaCadastroProduto extends JFrame {

	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	private JButton voltarButton;
	
	private JLabel codigoLabel;
	private JTextField codigoTextField;
	
	private JLabel nomeLabel;
	private JTextField nomeTextField;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private BancoDados bd = new BancoDados();
		
	public JanelaCadastroProduto() {
		super("Cadastro de Produtos");
		
		codigoLabel = new JLabel("Codigo");
		codigoTextField = new JTextField(10);
		
		nomeLabel = new JLabel("Nome do Produto");
		nomeTextField = new JTextField(30);
		
		cadastroPanel = new JPanel(new GridLayout(7,1));
		cadastroPanel.add(codigoLabel);
		cadastroPanel.add(codigoTextField);
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
		
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					bd.inserirProduto(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Produto inserido com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				nomeTextField.setText("");
				nomeTextField.setText(bd.listarProduto(codigoTextField.getText()));			
			}
			
		});		
		apagarButton = new JButton("Apagar Produto");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					bd.apagarProduto(codigoTextField.getText());
					codigoTextField.setText("");
					nomeTextField.setText("");
					JOptionPane.showMessageDialog(null,"Produto excluido com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
			}
			
		});		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					bd.atualizarProduto(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Produto atualizado com sucesso!!");					
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
				
			}
			
		});		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new JanelaListagemProduto();
			}
			
		});
		
		voltarButton = new JButton("Voltar");
		voltarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});		
		
		
		botoesPanel = new JPanel(new GridLayout(1,6));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		botoesPanel.add(voltarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(700,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(false);
		
	}
	
}
